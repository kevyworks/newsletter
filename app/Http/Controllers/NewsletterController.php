<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class NewsletterController extends Controller
{
    public function list(Request $request)
    {
        return response()->json(Subscriber::all());
    }

    public function index()
    {
        return View::make('newsletter');
    }

    public function store(Request $request)
    {
        // Check for uniques
        $validator = Validator::make($request->only(['email']), [
            'email' => 'required|email|unique:subscribers',
        ]);

        // Validate and create if unique. Account for database errors.
        $data = ['success' => false];

        try {
            if ($validator->fails()) {
                throw new \Exception($validator->errors()->first());
            }

            Subscriber::create([
                'email' => $request->get('email'),
            ]);

            $data['success'] = true;
        } catch(\Exception $e) {
            if (str_contains($e->getMessage(), 'SQLSTATE') && env('APP_DEBUG') !== true) {
                $data['error'] = 'Subscription was unsuccessful. Please try again later.';
            } else {
                $data['error'] = $e->getMessage();
            }
        }

        // Render view
        return View::make('newsletter', $data);
    }
}
