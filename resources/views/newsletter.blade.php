<!doctype html>
<html lang="en">
<head>
    <title>Subscribe</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Epilogue:wght@400;500&display=swap" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        * {
            font-family: 'Epilogue', sans-serif;
        }
    </style>
</head>
<body class="h-full w-full bg-fixed bg-cover bg-center" style="background-image:url(https://wikimediafoundation.org/wp-content/uploads/2022/01/header.svg)">

<div class="flex justify-center items-center h-screen">
    <div class="bg-white p-2 rounded-0 max-w-full drop-shadow-0 md:drop-shadow-xl md:max-w-2xl md:rounded-xl">
        <div class="p-8 text-center">
            <div class="flex flex-col items-center text-center">
                @if (empty($success))
                    <h1 class="text-2xl font-medium text-gray-900 mb-8">Get weekly email updates</h1>
                    <p class="text-slate-600 text-lg">Subscribe to news about ongoing projects and initiatives.</p>
                @else
                    <h1 class="text-2xl font-medium text-gray-900 mb-8">Success!</h1>
                    <p class="text-slate-600 text-lg">Thank you for subscribing to our weekly updates.</p>

                    <a href="{{ route('subscribe.index') }}" class="relative mt-8 flex justify-center py-2 px-8
                        font-medium rounded-lg text-white bg-pink-700
                        focus:outline-none hover:bg-pink-600 active:bg-pink-600">
                        Back to Home
                    </a>
                @endif
            </div>
            @if (empty($success))
                <p class="text-slate-900 mt-8 text-lg">Enter your email address</p>
                <form action="{{ route('subscribe.post') }}" method="POST" class="relative mt-3 flex flex-col items-center" autocomplete="off">
                    <input class="focus:ring-2 focus:ring-pink-700 focus:outline-none appearance-none
                            w-full text-md text-slate-900 rounded-md
                            py-4 px-4 ring-1 ring-slate-200 shadow-sm"
                           id="email"
                           autocomplete="off"
                           name="email"
                           type="email"
                           required/>

                    @if (isset($error))
                        <span class="text-red-700 mt-2">{!! $error !!}</span>
                    @endif

                    <button type="submit" class="relative mt-8 flex justify-center py-2 px-8
                        font-medium rounded-lg text-white bg-pink-700
                        focus:outline-none hover:bg-pink-600 active:bg-pink-600">
                        Subscribe
                    </button>
                </form>
            @endif
        </div>
    </div>
</div>
</body>
</html>
