<?php

use Illuminate\Http\Request;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json(['framework' => $router->app->version()]);
});

// /admin/list/?token=...
$router->group(['prefix' => 'admin', 'middleware' => 'adminToken'], function () use ($router) {
    $router->get('/list', ['uses' => 'NewsletterController@list', 'as' => 'admin.list']);
});

$router->get('/subscribe', ['uses' => 'NewsletterController@index', 'as' => 'subscribe.index']);
$router->post('/subscribe', ['uses' => 'NewsletterController@store', 'as' => 'subscribe.post']);
